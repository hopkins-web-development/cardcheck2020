const mongoose = require('mongoose');
const validator = require('validator');
const socialMediaNetworkOptions = [
    'Facebook',
    'Twitter',
    'Instagram',
    'LinkedIn'
]

const model = mongoose.model('Employee', {
  lastName: {
    type: String,
    required: true,
    validate: {
      validator(lastName) {
        return validator.isAlphanumeric(lastName);
      },
    },
  },
  firstName: {
    type: String,
    require: true,
    validate: {
      validator(firstName) {
        return validator.isAlphanumeric(firstName);
      },
    },
  },
  handle: {
    type: String,
    validate:{
        validators(handle){
            return validator.isURL(handle);
        },
    }
  },
  socialMediaNetwork: {
    type: String,
    validate:{
        validators(socialMediaNetwork){
            return validator.isIn(socialMediaNetwork,socialMediaNetworkOptions);
        },
    }
  },
  email: {
    type: String,
    required: false,
    validate: {
      validator(email) {
        return validator.isEmail(email);
      },
    },
  },
  phone: {
    type: String,
    required: true,
    validate: {
      validator(phone) {
        return validator.isMobilePhone(phone, ['en-US']);
      },
    },
  },
  amazon: {
    type: String,
    required: false,
    validate: {
      validator(amazon) {
        return validator.isEmail(amazon);
      },
    },
  },
  employer: {
    type: String,
    required: true,
    validate: {
      validator(employer) {
        return validator.isAlphanumeric(employer);
      },
    },
  },
  location: {
    type: String,
    required: true,
    validate: {
      validator(location) {
        return validator.isAlphanumeric(location);
      },
    },
  },
  organizer: {
    type: Boolean,
    required: true,
  },
  id: {
    type: mongoose.Schema.Types.ObjectId,
    validate: {
        validator(id) {
          return validator.isMongoId(id);
        },
      },
  },
});

module.exports = model;

