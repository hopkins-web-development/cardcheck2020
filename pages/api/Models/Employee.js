const dynamoose = require('dynamoose');
const validatorLib = require('mongoose-validator');
const socialMediaNetworkOptions = [
    '',
    'Facebook',
    'Twitter',
    'Instagram',
    'LinkedIn'
]
const Schema = dynamoose.Schema

const isAlpha = (inputtxt) =>
{
 let letters = /^[A-Za-z]+$/;
 if(inputtxt.split('').filter(char => char.match(letters)))
   {
    return true;
   }
 else
   {
   return false;
   }
}

const isEmail = (inputtxt) => {
  let letters = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
  if(inputtxt.match(letters)){
    return true
  } else {
    return false
  }
}

const isMobilePhone = (inputtxt) => {
  let letters = /^([0-1]([\s-./\\])?)?(\(?[2-9]\d{2}\)?|[2-9]\d{3})([\s-./\\])?(\d{3}([\s-./\\])?\d{4}|[a-zA-Z0-9]{7})$/
  if(inputtxt.match(letters)){
    return true
  } else {
    return false
  }
}

const employee = new Schema( {
  lastName: {
    type: String,
    required: true,
    validate:  (lastName) => isAlpha(lastName)
  },
  firstName: {
    type: String,
    require: true,
    validate: (firstName) => isAlpha(firstName)
  },
  handle: {
    type: String,
    validate: (handle) => isAlpha(handle)
  },
  socialMediaNetwork: {
    type: String,
    validate: (socialMediaNetwork) => socialMediaNetworkOptions.includes(socialMediaNetwork)
  },
  email: {
    type: String,
    required: true,
    validate: (email) => isEmail(email)
  },
  phone: {
    type: String,
    required: true,
    validate: (phone) => isMobilePhone(phone, ['en-US'])
  },
  employer: {
    type: String,
    required: true,
    validate: employer => isAlpha(employer)
  },
  location: {
    type: String,
    required: true,
    validate: location => isAlpha(location)
  },
  organizer: {
    type: Boolean,
    required: true,
  }
});

module.exports = employee;

