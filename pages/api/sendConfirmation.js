const mailjet = require ('node-mailjet')
.connect('74a4728475cf62bb1f4fa5df16635ebf', '732b6c51a9c91eeafef83f0241498207')

export default (employee, objectId) => { 
const getDate = () => new Date(Date.now()).toString()
  console.log('emp: '+employee)
return mailjet
.post("send", {'version': 'v3.1'})
.request({
  "Messages":[
    {
      "From": {
        "Email": "confirmations@dsf4union.org",
        "Name": "DSF4 Workers Union"
      },
      "To": [
        {
          "Email": `${employee.email}`,
          "Name": `${employee.firstName}`
        }
      ],
      "Subject": "Union Authorization Confirmation.",
      "TextPart": "Confirming your responses",
      "HTMLPart": 
        `<h3>Union Authorization Card Confirmation</h3><br />
        Dear ${employee.firstName}, here are the responses entered on the form at dsf4union.org: <br /> <br />
        First Name: ${employee.firstName} <br />
        Last Name: ${employee.lastName} <br />
        Email Address: ${employee.email} <br />
        Amazon ID: ${employee.amazon} <br />
        Social Media Handle: ${employee.handle} <br />
        Social Media Network: ${employee.network} <br />
        Phone: ${employee.phone} <br />
        Employer: ${employee.employer} <br />
        Location: ${employee.location} <br />
        Want to organize?: ${employee.organizer}?"Yes":"No"<br />
        Submitted At: ${getDate()} <br />

        If you notice any errors in the information above please contact corrections@dsf4union.org or paradoxp@protonmail.com.
        `,
      "CustomID": "AppGettingStartedTest"
    }
  ]
})
}

