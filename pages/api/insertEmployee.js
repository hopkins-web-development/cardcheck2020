const dynamoose = require('dynamoose');
require('dotenv').config()
const EmployeeSchema = require('./Models/Employee.js');
const EmployeeModel = dynamoose.model('Employee', EmployeeSchema)




export default (employee, callback) => {
  
    const Employee = new EmployeeModel({
        firstName: employee.firstName,
        lastName: employee.lastName,
        email: employee.email,
        handle: employee.handle,
        amazon: employee.amazon,
        network: employee.network,
        phone: employee.phone,        
        employer: employee.employer,
        location: employee.location,
        organizer: employee.organizer, 
    });
  
    Employee.save({}, callback)
  };
