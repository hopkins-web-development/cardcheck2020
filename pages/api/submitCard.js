'use strict';
import insertEmployee from './insertEmployee.js';
import sendConfirmation from './sendConfirmation.js';

//function which takes employeeData {} 
//inserts a new document into the db
//if successful send confirmation email
//if fail return error

const addEmployee =  (employeeData) => {
  return new Promise ((resolve, reject) => insertEmployee(employeeData, (err) => {
    if(err){
      console.log('addEmployee Error ', err)
      reject(err)
    } else {
      resolve(employeeData)
    }
  }) )
};

export default async (req, res) => {
  console.log(req.body)
    await addEmployee(req.body)
      .then(result => {
        sendConfirmation(result)
        res.status(200).json(result)
      })
      .catch(error => console.log(error))
  };