import React, { Component } from 'react'
import Router from 'next/router'


const UserContext = React.createContext()

class UserProvider extends Component {
  state = {
    language: false,
    firstName: '',
    lastName: '',
    email: '',
    handle: '',
    amazon: '',
    network: '',
    phone: '',
    employer: 'Amazon.com Services Inc.',
    location: 'DSF4 - 990 Beecher Street, San Leandro, California',
    organizer: false,
  }

  handleChange (e) {
      e.preventDefault()
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({ [name]: value });
  }

  changeLanguage (){ 
      console.log(this.state.language)
      this.setState({language: !this.state.language})
  }

  async handleSubmit(){
    try {
      const response = await fetch('api/submitCard', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(this.state)
      })
      if (response.ok) {
        console.log(response)
        Router.push('/thanks')
      } 
    } catch (error) {
      console.error(
        'You have an error in your code or there are Network issues.',
        error
      )
      throw new Error(error)
      }
    }  

  render () {
    return (
      <UserContext.Provider
        value={{
            language: this.state.language,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            handle: this.state.url,
            network: this.state.network,
            phone: this.state.phone,
            employer: this.state.employer,
            location: this.state.location,
            organizer: this.state.organizer,
            handleChange: this.handleChange.bind(this),
            changeLanguage: this.changeLanguage.bind(this),
            handleSubmit: this.handleSubmit.bind(this)
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    )
  }
}

const UserConsumer = UserContext.Consumer

export default UserProvider
export { UserConsumer, UserContext }