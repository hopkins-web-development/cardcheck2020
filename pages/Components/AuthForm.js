import {Component} from 'react'
import Link from 'next/link'

export default class AuthForm extends React.Component {
    constructor (props){
        super(props);
        this.state = {
            firstName: props.firstName,
            lastName: props.lastName,
            email: props.email,
            handle: props.handle,
            network: props.network,
            phone: props.phone,
            address: props.address,
            employer: props.employer,
            location: props.location,
            title: props.title, 
            organizer: props.organizer, 
        }
       this.handleChange = props.handleChange
    }

    render (){
        return (
            <form onSubmit={this.handleChange}>
                <label>
                First Name:
                <input type="text" name='firstName' defaultValue={this.state.firstName} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Last Name:
                <input type="text" name='lastName' defaultValue={this.state.lastName} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Email Address:
                <input type="text" name='email' defaultValue={this.state.email} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Social Media Handle or URL (optional):
                <input type="text" name='handle' defaultValue={this.state.handle} onChange={e => this.handleChange(e)} />
                </label>

                <br></br>
                <label>
                Social media Network:
                <select name='network' defaultValue={this.state.network}  onChange={e => this.handleChange(e)}>
                    <option value="this.state.facebook">Facebook</option>
                    <option value="this.state.twitter">Twitter</option>
                    <option value="this.state.instagram">Instagram</option>
                    <option value="this.state.linkedin">LinkedIn</option>
                </select>
                </label>
                <br></br>

                <label> 
                Phone Number:
                <input type="text" name='phone' defaultValue={this.state.phone} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Home Address:
                <input type="text" name='address' defaultValue={this.state.address} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Employer:
                <input type="text" name='employer' defaultValue={this.state.employer} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Work Location:
                <input type="text" name='location' defaultValue={this.state.location} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Title:
                <input type="text" name='title' defaultValue={this.state.title} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                    I'd like to participate in the Union Organizing Committee:
                <input type="checkbox" name='organizer' defaultValue={this.state.organizer} onChange={e => this.handleChange(e)}/>
                </label>
                <br></br>
                <br></br>
            </form>
        )
    }
}