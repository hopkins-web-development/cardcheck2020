import React from 'react'
import Link from 'next/link'
import {UserConsumer, UserContext} from './Components/UserContext';
import {styled} from '@material-ui/core/styles'
import { Card, Input, InputLabel, Checkbox, Button, Container, Select, Paper, FormControl, FormLabel, FormHelperText, FormGroup, MenuItem} from '@material-ui/core'

const StyledSelect = styled(Select)({
    minWidth: '100px'
  });


export default class AuthForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      firstName: '',
      lastName: '',
      email: '',
      amazon: '',
      handle: '',
      network: '',
      phone: '',
      employer: '',
      location: ''
    };

  }

  validateForm(event) {
    if(this.state.handle === '' && this.state.email === '' && this.state.amazon === ''){
      console.log('a thing happened')
    }

  }


  render() {
    this.state = this.context
    this.handleChange = this.context.handleChange
    this.handleSubmit = this.context.handleSubmit
    return (
      <Container maxWidth='xs'>
      <div>
      <Card>
        <Paper variant="outlined"> <p>I hereby suport the creation of a union of employees for the purposes of collective bargaining at my workplace. Upon the creation of such a union, I authorize it to bargain with my employer on my behalf.</p> </Paper>
        <br></br>
        
        <form onSubmit={this.handleSubmit}>
        <Card>
        <FormControl required={true}>
          <InputLabel> First Name: </InputLabel>
          <Input type="text" name='firstName' defaultValue={this.state.firstName} onChange={e => this.context.handleChange(e)} />
        </FormControl>

        <br></br>
        
        <FormControl required={true}>
          <InputLabel> Last Name: </InputLabel>
          <Input type="text" name='lastName' defaultValue={this.state.lastName} onChange={e => this.handleChange(e)} />
        </FormControl>
        </Card>
       

        <br></br>
        <Paper>
        <FormControl >
          <InputLabel>Personal Email:</InputLabel>
          <Input type="text" name='email' defaultValue={this.state.email} onChange={e => this.handleChange(e)} />
        </FormControl>

          <br></br>
        <FormControl>
          <InputLabel>Amazon Login Email:</InputLabel>
          <Input type="text" name='amazon' defaultValue={this.state.email} onChange={e => this.handleChange(e)} />
          <FormHelperText id="Amazon">The one you type in to log into Dolphin or SSP. eg.: 'bretonn@amazon.com'</FormHelperText>
        </FormControl>

        <br></br>
        <Card>
        <FormControl>
          <InputLabel> Social Media Handle:</InputLabel>
          <Input type="text" name='handle' defaultValue={this.state.handle} onChange={e => this.handleChange(e)} />
          <FormHelperText id="handle">Like @DSF4UNION</FormHelperText>
        </FormControl>
        <br></br>
        
        

        <br></br>
        <FormControl fullWidth={true} required={ this.state.handle ? true : false }>
        <FormLabel>Network:</FormLabel>
          <StyledSelect name='network' variant='outlined' value={this.state.network}  onChange={e => this.handleChange(e)}>
            <MenuItem value="">None</MenuItem>
            <MenuItem value="facebook">Facebook</MenuItem>
            <MenuItem value="twitter">Twitter</MenuItem>
            <MenuItem value="instagram">Instagram</MenuItem>
            <MenuItem value="linkedin">LinkedIn</MenuItem>
          </StyledSelect>
        </FormControl>
        </Card>
        </Paper>
        

          <br></br>
          <br></br>

          <Paper>
          <FormControl required={true}>
          <InputLabel>Phone Number: </InputLabel>
          <Input modifier='material' type="text" name='phone' defaultValue={this.state.phone} onChange={e => this.handleChange(e)} />
          </FormControl>

        <br></br>

        <br></br>
        <FormControl required={true}>
          <InputLabel>Employer:</InputLabel>
          <Input type="text" name='employer' defaultValue={this.state.employer} onChange={e => this.handleChange(e)} />   
          <FormHelperText id="handle"> Amazon.com Services Inc. </FormHelperText>
        </FormControl>

        <br></br>
        <FormControl>
          <InputLabel>Work Location:</InputLabel>
          <Input type="text" name='location' defaultValue={this.state.location} onChange={e => this.handleChange(e)} />
          
        </FormControl>

        

          <br></br>
          <FormGroup>
          <FormHelperText id="handle">I'd like to participate in the Union Organizing Committee: </FormHelperText>
              <Checkbox name='organizer' value={this.state.organizer} onChange={e => this.handleChange(e)}/>
          </FormGroup>
          
        </Paper>
          
          <br></br>
          <br></br>
          
            <Button size='large' type="submit" variant="contained" defaultValue="Submit" color="primary" onSubmit={e => this.validateForm(e)}> 
            <Link href='Confirm'><a>Sign Union Authorization</a></Link> 
            </Button>
             
        </form>
        </Card>
        
      </div>
     </Container> 
    );
  }
}

AuthForm.contextType = UserContext
